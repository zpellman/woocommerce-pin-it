<?php
/**
 * Plugin Name: WooCommerce Pin-It!
 * Plugin URI: http://www.zachpellman.com/projects/woocommerce-pinit
 * Description: Adds a Pin It! button to the WooCommerce product page
 * Version: 1.0
 * Author: Zach Pellman <zpellman@gmail.com>
 * Author URI: http://www.zachpellman.com
 * License: GPL2
 *
 * Copyright 2013 Zach Pellman <zpellman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

// sets the base URL for creating a Pinterest pin
if ( ! defined( 'PINIT_URL' ) ) {
	define( 'PINIT_URL', '//pinterest.com/pin/create/button/?' );
}

/**
 * Initializes the Pin It! button
 */
function pinit_init_button() {
	wp_enqueue_script( 'pinterest', '//assets.pinterest.com/js/pinit.js', array( 'jquery' ) );
	wp_enqueue_script( 'pinit', plugins_url( '/pinit.js', __FILE__ ), array( 'jquery' ) );
}

/**
 * Adds a Pin It! Button sub-menu to the WooCommerce menu
 */
function pinit_admin_menu() {
	add_submenu_page( 'woocommerce', 'Pin It! Options', 'Pin It! Options', 'manage_options', 'pinit-options', 'pinit_display_settings' );
}

/**
 * Displays the Pin It! button
 */
function pinit_display_button() {
	// read the existing value from the database
	$suffix = get_option( 'pinit-description-suffix' );

	// build the HTTP query
	$query = http_build_query( array(
		'url'         => get_permalink(),
		'media'       => wp_get_attachment_url( get_post_thumbnail_id() ),
		'description' => get_the_title() . $suffix
	) );

	// prepares the full link href
	$href = PINIT_URL . $query;

	// displays the Pin It! button
	require_once( 'pinit-button.php' );
}

/**
 * Displays the Pin It! button settings page
 */
function pinit_display_settings() {
	// checks that the user has the required capability
	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( __( 'You do not have sufficient permissions to access this page' ) );
	}

	// read the existing value from the database
	$suffix = get_option( 'pinit-description-suffix' );

	// checks if the user has updated the description suffix
	if ( isset( $_POST[ 'pinit-description-suffix' ] ) ) {
		// gets the updated value
		$suffix = $_POST[ 'pinit-description-suffix' ];

		// saves the updated value in the database
		update_option( 'pinit-description-suffix', $suffix );

		// displays a success message
		require_once( 'pinit-admin-success.php' );
	}

	// displays the settings form
	require_once( 'pinit-admin.php' );
}

/**
 * Displays a "Settings" link on the plugins page
 */
function pinit_settings_link( $links ) {
	$settings_link = '<a href="admin.php?page=pinit-options">' . __( 'Settings', 'woocommerce-pinit' ) . '</a>';

	array_unshift($links, $settings_link);
	return $links;
}

/**
 * Binds WordPress and WooCommerce action hooks
 */
if ( is_admin() ) {
	$plugin = plugin_basename(__FILE__);

	add_action( 'admin_menu', 'pinit_admin_menu' );
	add_filter( 'plugin_action_links_' . $plugin, 'pinit_settings_link' );
} else {
	add_action( 'wp_enqueue_scripts', 'pinit_init_button' );

	/* 30 adds it after the Add to Cart button */
	add_action( 'woocommerce_single_product_summary', 'pinit_display_button', 6 );
}