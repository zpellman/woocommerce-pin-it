=== WooCommerce Pin It! ===
Contributors: Zach Pellman
Tags: woocommerce, pinterest
Requires at least: 3.5.1
Tested up to: 3.5.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds a Pinterest Pin It! plugin to WooCommerce product pages.

== Description ==

Adds a Pinterest Pin It! plugin to WooCommerce product pages.

== Installation ==

1. Upload `plugin-name.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.0 =
* First stable release
