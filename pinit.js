(function($) {
	$(document).ready(function() {
		/*
		 * Displays the Pin It! dialog in a new window
		 */
		$(".pinit").on('click', function(e) {
			var href;

			// prevents the default link behavior
			e.preventDefault();

			// gets the current link href
			href = $(this).attr('href');

			// opens the link in a pop-up window
			window.open(href, 'Pinterest', 'height=286,width=750,scrollbars=no');
		});
	});
})(jQuery);