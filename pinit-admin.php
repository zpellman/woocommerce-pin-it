<div class="wrap">
	<h2><?php _e( 'Pin It! Button Settings', 'woocommerce-pinit' ); ?></h2>
	<h3><?php _e( 'Description Suffix', 'woocommerce-pinit' ); ?></h3>
	<p><?php _e( 'This text will be added after the product title in the Pinterest description.', 'woocommerce-pinit' ); ?></p>
	<form name="pinit-button-settings" method="post" action="">
		<textarea name="pinit-description-suffix" class="large-text code" rows="3"><?php
			echo stripslashes( esc_textarea( $suffix ) );
		?></textarea>
		<p class="submit">
			<input type="submit" name="submit" class="button button-primary" value="<?php esc_attr_e( 'Save Changes' ); ?>" />
		</p>
	</form>
</div>